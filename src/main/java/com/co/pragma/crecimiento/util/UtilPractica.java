package com.co.pragma.crecimiento.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UtilPractica {

	public String formatMessage(BindingResult result) {
		List<Map<String,String>> errores = result.getFieldErrors().stream()
				.map(err->{
					Map<String,String> error = new HashMap<>();
					error.put(err.getField(), err.getDefaultMessage());
					return error;
				}).collect(Collectors.toList());
		
		ErrorMessage errorMensaje = ErrorMessage.builder()
				.codigo("01")
				.mensaje(errores).build();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString="";
		try {
			jsonString = mapper.writeValueAsString(errorMensaje);
		}catch(JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonString;
	}
}
