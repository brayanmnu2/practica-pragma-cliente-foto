package com.co.pragma.crecimiento.util;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.model.ClienteFotoEntity;

public class DatosTest {
	public static final Long UNO_LONG = 1L;
	public static final Long DOS_LONG = 2L;
	public static final Long TRES_LONG = 3L;
	
	public static final ClienteFotoEntity CLIENTE_FOTO_ENTITY_001 = new ClienteFotoEntity(UNO_LONG, "imagen1 en base64");
	public static final ClienteFotoEntity CLIENTE_FOTO_ENTITY_MODIFICADO_001 = new ClienteFotoEntity(UNO_LONG, "imagen123 en base64");
	public static final ClienteFotoEntity CLIENTE_FOTO_ENTITY_SIN_ID_001 = new ClienteFotoEntity(null, "imagen1 en base64");
	public static final ClienteFotoEntity CLIENTE_FOTO_ENTITY_002 = new ClienteFotoEntity(DOS_LONG, "imagen2 en base64");
	public static final ClienteFotoEntity CLIENTE_FOTO_ENTITY_003 = new ClienteFotoEntity(TRES_LONG, "image3 en base64");
	
	public static final Optional<ClienteFotoEntity> CLIENTE_FOTO_ENTITY_OPTIONAL_001 = Optional.of(CLIENTE_FOTO_ENTITY_001);
	
	public static final List<ClienteFotoEntity> LIST_CLIENTE_001 = Arrays.asList(CLIENTE_FOTO_ENTITY_001,CLIENTE_FOTO_ENTITY_002,CLIENTE_FOTO_ENTITY_003);
	
	
	public static final ClienteFotoDto CLIENTE_FOTO_DTO_REGISTRO_001 = new ClienteFotoDto(UNO_LONG, "imagen1 en base64");
	public static final ClienteFotoDto CLIENTE_FOTO_DTO_REGISTRO_SN_VALID_001 = new ClienteFotoDto(null, "imagen1 en base64");
	public static final ClienteFotoDto CLIENTE_FOTO_DTO_MODIFICADO_001 = new ClienteFotoDto(UNO_LONG, "imagen123 en base64");
	public static final ClienteFotoDto CLIENTE_FOTO_DTO_MODIFICADO_SIN_VALID_001 = new ClienteFotoDto(null, "imagen123 en base64");
	
	
	public static final String IMG_001 =  "imagen1 en base64";
	public static final String IMG_MODIFICADO_001 =  "imagen123 en base64";
	
	public static final Optional<ClienteFotoEntity> FOTO_ENTITY_OPTIONAL_EMPTY = Optional.empty();
}
