package com.co.pragma.crecimiento.model.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.co.pragma.crecimiento.model.ClienteFotoEntity;

@Repository
public interface ClienteFotoRepository extends MongoRepository<ClienteFotoEntity, Long>{

}
