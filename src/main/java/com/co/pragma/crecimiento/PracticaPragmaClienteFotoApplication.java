package com.co.pragma.crecimiento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@SpringBootApplication
public class PracticaPragmaClienteFotoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaPragmaClienteFotoApplication.class, args);
	}

}
