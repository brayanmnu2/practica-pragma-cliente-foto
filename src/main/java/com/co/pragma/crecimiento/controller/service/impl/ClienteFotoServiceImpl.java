package com.co.pragma.crecimiento.controller.service.impl;

import java.util.Base64;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.pragma.crecimiento.controller.service.ClienteFotoService;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.mapper.ClienteFotoMapper;
import com.co.pragma.crecimiento.model.ClienteFotoEntity;
import com.co.pragma.crecimiento.model.repository.ClienteFotoRepository;

@Service
public class ClienteFotoServiceImpl implements ClienteFotoService{

	private static final Logger logger = LogManager.getLogger(ClienteFotoServiceImpl.class);

	@Autowired
	private ClienteFotoRepository clienteFotoRepository;
	
	@Autowired
	private ClienteFotoMapper mapper;
	
	@Override
	public ClienteFotoDto registrarClienteFoto(ClienteFotoDto clienteFoto) {
		ClienteFotoDto busqueda = verClienteFotoPorId(clienteFoto.getId());
		if(null!=busqueda) {
			return null;
		}
		
		logger.info("imagen en String:"+clienteFoto.getImg());
		logger.info("imagen en Base64:"+
				Base64.getEncoder().encodeToString(clienteFoto.getImg().getBytes()));
		
		ClienteFotoEntity entity = mapper.clienteFotoDtoToClienteFotoEntity(clienteFoto);
		entity = clienteFotoRepository.save(entity);
		return mapper.clienteFotoEntityToClienteFotoDto(entity);
	}

	@Override
	public List<ClienteFotoDto> listarClienteFoto() {
		List<ClienteFotoEntity> listClienteFotoEntity = clienteFotoRepository.findAll();
		return mapper.listClienteFotoEntityToListClienteFotoDto(listClienteFotoEntity);
	}

	@Override
	public ClienteFotoDto actualizarClienteFoto(ClienteFotoDto clienteDto) {
		ClienteFotoDto busqueda = verClienteFotoPorId(clienteDto.getId());
		if(null==busqueda) {
			return null;
		}
		ClienteFotoEntity clienteFotoEntity = mapper.clienteFotoDtoToClienteFotoEntity(clienteDto);
		clienteFotoEntity = clienteFotoRepository.save(clienteFotoEntity);
		return mapper.clienteFotoEntityToClienteFotoDto(clienteFotoEntity);
	}

	@Override
	public boolean eliminarClienteFoto(Long idCliente) {
		ClienteFotoDto busqueda = verClienteFotoPorId(idCliente);
		if(null==busqueda) {
			return false;
		}
		ClienteFotoEntity clienteFotoEntity = mapper.clienteFotoDtoToClienteFotoEntity(busqueda);
		clienteFotoRepository.delete(clienteFotoEntity);
		return true;
	}

	@Override
	public ClienteFotoDto verClienteFotoPorId(Long idCliente) {
		ClienteFotoEntity clienteFotoEntity = clienteFotoRepository.findById(idCliente).orElse(null);
		return mapper.clienteFotoEntityToClienteFotoDto(clienteFotoEntity);
	}

}
