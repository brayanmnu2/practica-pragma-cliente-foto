package com.co.pragma.crecimiento.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.co.pragma.crecimiento.controller.service.ClienteFotoService;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.util.UtilPractica;

@RestController
@RequestMapping(value="cliente-foto")
public class ClienteFotoController {
	
	
	@Autowired
	private ClienteFotoService clienteFotoService;
	
	@PostMapping(value="registrar")
	public ResponseEntity<ClienteFotoDto> registrarFoto(@Valid @RequestBody ClienteFotoDto clienteFotoDto/*, BindingResult result*/){

		ClienteFotoDto clienteFotoReturn = clienteFotoService.registrarClienteFoto(clienteFotoDto);
		if(null==clienteFotoReturn) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteFotoReturn);
	}
	
	@GetMapping(value="listarFotoClientes")
	public ResponseEntity<List<ClienteFotoDto>> listarFotos() {
		List<ClienteFotoDto> listaClientesFoto = clienteFotoService.listarClienteFoto();
		if(null==listaClientesFoto || listaClientesFoto.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(listaClientesFoto);
	}
	
	@PutMapping(value="actualizar")
	public ResponseEntity<ClienteFotoDto> actualizarFoto(@Valid @RequestBody ClienteFotoDto clienteFotoDto){
		if(null!=clienteFotoDto.getId()) {
			ClienteFotoDto clienteFotoReturn = clienteFotoService.actualizarClienteFoto(clienteFotoDto);
			if(null!=clienteFotoReturn) {
				return ResponseEntity.ok(clienteFotoReturn);
			}
		}
		return ResponseEntity.badRequest().build();
		
	}
	
	@DeleteMapping(value="eliminar/{id}")
	public ResponseEntity<ClienteFotoDto> eliminarFoto(@PathVariable("id") Long id){
		boolean isEliminado = clienteFotoService.eliminarClienteFoto(id);
		if(!isEliminado) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(null);
	}
	
	@GetMapping(value="buscarFotoPorIdCliente/{idCliente}")
	public ResponseEntity<ClienteFotoDto> buscarFotoPorIdCliente(@PathVariable("idCliente") Long idCliente){
		ClienteFotoDto fotoCliente = clienteFotoService.verClienteFotoPorId(idCliente);
		if(null==fotoCliente) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(fotoCliente);
	}
	

}
