package com.co.pragma.crecimiento.controller.service;

import java.util.List;

import com.co.pragma.crecimiento.dto.ClienteFotoDto;

public interface ClienteFotoService {

	public ClienteFotoDto registrarClienteFoto (ClienteFotoDto clienteFoto);
	
	public List<ClienteFotoDto> listarClienteFoto();
	
	public ClienteFotoDto actualizarClienteFoto(ClienteFotoDto clienteDto);
	
	public boolean eliminarClienteFoto(Long idCliente);
	
	public ClienteFotoDto verClienteFotoPorId(Long idCliente);
	
}
