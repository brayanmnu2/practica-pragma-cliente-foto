package com.co.pragma.crecimiento.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClienteFotoDto {

	@NotNull(message = "El id no puede ser vacío")
	private Long id;
	
	private String img;
}
