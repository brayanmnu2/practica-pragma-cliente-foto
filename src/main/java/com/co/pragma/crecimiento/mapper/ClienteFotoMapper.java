package com.co.pragma.crecimiento.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.model.ClienteFotoEntity;

@Mapper(componentModel = "spring")
public interface ClienteFotoMapper {

	ClienteFotoDto clienteFotoEntityToClienteFotoDto(ClienteFotoEntity clienteFotoEntity);

	ClienteFotoEntity clienteFotoDtoToClienteFotoEntity(ClienteFotoDto clienteFotoDto);

	List<ClienteFotoEntity> listClienteFotoDtoToListClienteFotoEntity(List<ClienteFotoDto> listClienteFotoDto);

	List<ClienteFotoDto> listClienteFotoEntityToListClienteFotoDto(List<ClienteFotoEntity> listClienteFotoEntity);
}
