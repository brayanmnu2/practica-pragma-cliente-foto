package com.co.pragma.crecimiento;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.co.pragma.crecimiento.controller.ClienteFotoController;
import com.co.pragma.crecimiento.controller.service.ClienteFotoService;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.mapper.ClienteFotoMapper;
import com.co.pragma.crecimiento.util.DatosTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ClienteFotoController.class)
public class ClienteFotoControllerTest {

	ObjectMapper objectMapper;

	@MockBean
	private ClienteFotoService fotoService;
	
	@Autowired
	private MockMvc mvc;
	
	@Spy
	ClienteFotoMapper fotoMapper = Mappers.getMapper(ClienteFotoMapper.class);
	
	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
	}
	
	@Test
	void testRegistrarFotoExitoso() throws JsonProcessingException, Exception {
		when(fotoService.registrarClienteFoto(any())).then(invocation ->{
            ClienteFotoDto f = invocation.getArgument(0);
            return f;
        });
		mvc.perform(post("/cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001)))
		.andExpect(status().isCreated())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001)));
		
		verify(fotoService).registrarClienteFoto(any());
	}
	
	@Test
	void testRegistrarFotoNoExitoso() throws JsonProcessingException, Exception {
		when(fotoService.registrarClienteFoto(any())).thenReturn(null);
		
		mvc.perform(post("/cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001)))
		.andExpect(status().isBadRequest());
		
		verify(fotoService).registrarClienteFoto(any());
	}
	
	@Test
	void testRegistrarFotoNoValid() throws JsonProcessingException, Exception {
		when(fotoService.registrarClienteFoto(any())).thenReturn(null);
		
		mvc.perform(post("/cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_SN_VALID_001)))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	void testListarFotosExitoso() throws JsonProcessingException, Exception {
		List<ClienteFotoDto> listFotoDto = fotoMapper.listClienteFotoEntityToListClienteFotoDto(DatosTest.LIST_CLIENTE_001);
		when(fotoService.listarClienteFoto()).thenReturn(listFotoDto);
		
		mvc.perform(get("/cliente-foto/listarFotoClientes").contentType(MediaType.APPLICATION_JSON))
				// Then
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].id").value("1"))
				.andExpect(jsonPath("$[1].id").value("2"))
				.andExpect(jsonPath("$[2].id").value("3"))
				.andExpect(jsonPath("$[0].img").value("imagen1 en base64"))
				.andExpect(jsonPath("$[1].img").value("imagen2 en base64"))
				.andExpect(jsonPath("$[2].img").value("image3 en base64"))
				.andExpect(jsonPath("$", hasSize(3)))
				//comparacion mas robusta
				.andExpect(content().json(objectMapper.writeValueAsString(listFotoDto)));
		verify(fotoService).listarClienteFoto();
	}
	
	
	@Test
	void testListarFotosNoExitoso() throws JsonProcessingException, Exception {
		when(fotoService.listarClienteFoto()).thenReturn(null);
		
		mvc.perform(get("/cliente-foto/listarFotoClientes").contentType(MediaType.APPLICATION_JSON))
				// Then
				.andExpect(status().isNoContent());
		verify(fotoService).listarClienteFoto();
	}
	
	@Test
	void testActualizarFotoExitoso() throws JsonProcessingException, Exception {
		when(fotoService.actualizarClienteFoto(any())).then(invocation ->{
            ClienteFotoDto f = invocation.getArgument(0);
            return f;
        });
		mvc.perform(put("/cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_001)))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_001)));
		
		verify(fotoService).actualizarClienteFoto(any());
	}
	
	@Test
	void testActualizarFotoNoExitoso() throws JsonProcessingException, Exception {
		when(fotoService.actualizarClienteFoto(any())).thenReturn(null);
		mvc.perform(put("/cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_001)))
		.andExpect(status().isBadRequest());
		
		verify(fotoService).actualizarClienteFoto(any());
	}
	
	@Test
	void testActualizarFotoNoValid() throws JsonProcessingException, Exception {
		when(fotoService.actualizarClienteFoto(any())).thenReturn(null);
		mvc.perform(put("/cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_SIN_VALID_001)))
		.andExpect(status().isBadRequest());
		;
	}
	
	@Test
	void testEliminarFotoExitoso() throws Exception {
		when(fotoService.eliminarClienteFoto(anyLong())).thenReturn(true);
		
		mvc.perform(delete("/cliente-foto/eliminar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
		
		verify(fotoService).eliminarClienteFoto(anyLong());
	}
	
	
	@Test
	void testEliminarFotoNoExitoso() throws Exception {
		when(fotoService.eliminarClienteFoto(anyLong())).thenReturn(false);
		
		mvc.perform(delete("/cliente-foto/eliminar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
		
		verify(fotoService).eliminarClienteFoto(anyLong());
	}
	
	@Test
	void testBuscarFotoPorIdClienteExitoso() throws JsonProcessingException, Exception {
		when(fotoService.verClienteFotoPorId(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001);
		
		mvc.perform(get("/cliente-foto/buscarFotoPorIdCliente/1").contentType(MediaType.APPLICATION_JSON))
		// Then
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.id").value("1"))
		.andExpect(jsonPath("$.img").value("imagen1 en base64"))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001)));
		
		verify(fotoService).verClienteFotoPorId(anyLong());
	}
}
