package com.co.pragma.crecimiento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.co.pragma.crecimiento.controller.service.impl.ClienteFotoServiceImpl;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.mapper.ClienteFotoMapper;
import com.co.pragma.crecimiento.model.repository.ClienteFotoRepository;
import com.co.pragma.crecimiento.util.DatosTest;

@SpringBootTest
public class ClienteFotoServiceTest {

	@Mock
	ClienteFotoRepository fotoRepository;

	@Spy
	ClienteFotoMapper fotoMapper = Mappers.getMapper(ClienteFotoMapper.class);

	@InjectMocks
	ClienteFotoServiceImpl fotoService;

	@Test
	void testRegistrarClienteFotoNoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_OPTIONAL_001);
		ClienteFotoDto fotoReturn = fotoService.registrarClienteFoto(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001);
		
		assertNull(fotoReturn);
	}
	
	@Test
	void testRegistrarClienteFotoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.FOTO_ENTITY_OPTIONAL_EMPTY);
		when(fotoRepository.save(DatosTest.CLIENTE_FOTO_ENTITY_001)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_001);
		ClienteFotoDto fotoReturn = fotoService.registrarClienteFoto(DatosTest.CLIENTE_FOTO_DTO_REGISTRO_001);
		
		assertNotNull(fotoReturn);
		assertEquals(DatosTest.IMG_001, fotoReturn.getImg());
	}
	
	@Test
	void testListarClienteFoto() {
		when(fotoRepository.findAll()).thenReturn(DatosTest.LIST_CLIENTE_001);

		List<ClienteFotoDto> listFotoReturn = fotoService.listarClienteFoto();

		assertNotNull(listFotoReturn);
		assertThat(listFotoReturn).isNotEmpty();
		assertTrue(listFotoReturn.stream().anyMatch(foto -> "imagen2 en base64".equals(foto.getImg())));
	}
	
	@Test
	void testActualizarClienteFotoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_OPTIONAL_001);
		when(fotoRepository.save(DatosTest.CLIENTE_FOTO_ENTITY_MODIFICADO_001)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_MODIFICADO_001);
		
		ClienteFotoDto fotoReturn = fotoService.actualizarClienteFoto(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_001);
		
		assertNotNull(fotoReturn);
		assertEquals(DatosTest.IMG_MODIFICADO_001, fotoReturn.getImg());
	}
	
	@Test
	void testActualizarClienteFotoNoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.FOTO_ENTITY_OPTIONAL_EMPTY);
		
		ClienteFotoDto fotoReturn = fotoService.actualizarClienteFoto(DatosTest.CLIENTE_FOTO_DTO_MODIFICADO_001);
		
		assertNull(fotoReturn);
		
	}
	
	@Test
	void testEliminarClienteFotoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_OPTIONAL_001);
		boolean isEliminado = fotoService.eliminarClienteFoto(DatosTest.UNO_LONG);
		assertTrue(isEliminado);
		verify(fotoRepository).findById(anyLong());
	}
	
	@Test
	void testEliminarClienteFotoNoExitoso() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.FOTO_ENTITY_OPTIONAL_EMPTY);
		boolean isEliminado = fotoService.eliminarClienteFoto(DatosTest.UNO_LONG);
		assertFalse(isEliminado);
	}
	
	@Test
	void testVerClienteFotoPorId() {
		when(fotoRepository.findById(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_FOTO_ENTITY_OPTIONAL_001);

		ClienteFotoDto clienteReturn = fotoService.verClienteFotoPorId(DatosTest.UNO_LONG);

		assertNotNull(clienteReturn);
		assertEquals(DatosTest.IMG_001, clienteReturn.getImg());

	}
}
